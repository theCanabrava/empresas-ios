//
//  APIConstants.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 24/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

struct APIConstants
{
    static let host = "https://empresas.ioasys.com.br/api/"
    static let apiVersion = "v1"
    
    static let post = "POST"
    static let get = "GET"
    
    static let applicationJson = "application/json"
    static let contentType = "Content-Type"
    static let accessToken = "access-token"
    static let client = "client"
    static let uid = "uid"
    
    static let pathSgnIn = "/users/auth/sign_in"
    static let pathEnterpriseIndex = "/enterprises"
    static let pathShow = "/enterprises/"
    
    static let emailField = "email"
    static let passwordField = "password"
    
    static let enterpriseTypeParam = "enterprise_types"
    static let nameParam = "name"
}
