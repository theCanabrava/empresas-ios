//
//  APILibrary.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 24/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

typealias SignInHandler = (Bool) -> Void
typealias EnterpriseIndexHandler = (EnterpriseList) -> Void
typealias ShowHandler = (Enterprise?) -> Void

class APIManager: NSObject, AuthProtocol, DataProtocol
{
    var accessToken: String = ""
    var client: String = ""
    var uid: String = ""
    
    
    
    public func signIn(credentials: SignInCredentials, onFinish: @escaping SignInHandler)
    {
        let url = URL(string: "\(APIConstants.host)\(APIConstants.apiVersion)\(APIConstants.pathSgnIn)")!
        let body = makeSignInBody(credentials)
        let request = makeRequest(url, APIConstants.post)
        
        let task = URLSession.shared.uploadTask(with: request, from: body) { (data, response, error) in
            if(error == nil)
            {
                self.signInResponse(response as! HTTPURLResponse, onFinish: onFinish)
            }
            else
            {
                onFinish(false)
            }
        }
        
        task.resume()
    }
    
    private func makeRequest(_ url: URL, _ method: String) -> URLRequest
    {
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.setValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.setValue(accessToken, forHTTPHeaderField: APIConstants.accessToken)
        request.setValue(client, forHTTPHeaderField: APIConstants.client)
        request.setValue(uid, forHTTPHeaderField: APIConstants.uid)
        return request
    }
    
    private func makeSignInBody(_ credentials: SignInCredentials) -> Data
    {
        let rawBody =
        [
            "\(APIConstants.emailField)": "\(credentials.email)",
            "\(APIConstants.passwordField)": "\(credentials.password)"
        ]
        return try! JSONSerialization.data(withJSONObject: rawBody, options: [])
    }
    
    private func signInResponse(_ response: HTTPURLResponse, onFinish: SignInHandler)
    {
        if(response.statusCode == 200)
        {
            saveUser(response)
            onFinish(true)
        }
        else
        {
            onFinish(false)
        }
    }
    
    private func saveUser(_ response: HTTPURLResponse)
    {
        accessToken = response.allHeaderFields[APIConstants.accessToken] as! String
        client = response.allHeaderFields[APIConstants.client] as! String
        uid = response.allHeaderFields[APIConstants.uid] as! String
    }
    
    

    public func enterpriseIndex(name: String, onFinish: @escaping EnterpriseIndexHandler)
    {
        let escapedName = name.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: "\(APIConstants.host)\(APIConstants.apiVersion)\(APIConstants.pathEnterpriseIndex)?\(APIConstants.nameParam)=\(escapedName)")!
        let request = makeRequest(url, APIConstants.get)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if(error == nil)
            {
                self.enterpriseIndexResponse(data!, response as! HTTPURLResponse, onFinish)
            }
            else
            {
                onFinish(EnterpriseList(enterprises: []))
            }
        }
        task.resume()
    }
    
    public func enterpriseIndexWithFilter(params: EnterpriseParameters, onFinish: @escaping EnterpriseIndexHandler)
    {
        let escapedName = params.name.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: "\(APIConstants.host)\(APIConstants.apiVersion)\(APIConstants.pathEnterpriseIndex)?\(APIConstants.enterpriseTypeParam)=\(params.enterpriseTypes)&\(APIConstants.nameParam)=\(escapedName)")!
        let request = makeRequest(url, APIConstants.get)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if(error == nil)
            {
                self.enterpriseIndexResponse(data!, response as! HTTPURLResponse, onFinish)
            }
            else
            {
                onFinish(EnterpriseList(enterprises: []))
            }
        }
        task.resume()
    }
    
    private func enterpriseIndexResponse(_ data: Data, _ response:HTTPURLResponse, _ onFinish: EnterpriseIndexHandler)
    {
        if(response.statusCode == 200)
        {
            let enterpriseList = EnterpriseListConverter.convert(json: data)
            onFinish(enterpriseList)
        }
        else
        {
            onFinish(EnterpriseList(enterprises: []))
        }
        
    }

    
    
    public func show(id: Int, onFinish: @escaping ShowHandler)
    {
        let url = URL(string: "\(APIConstants.host)\(APIConstants.apiVersion)\(APIConstants.pathShow)\(id)")!
        let request = makeRequest(url, APIConstants.get)
        let task = URLSession.shared.dataTask(with: request)
        {
            (data, response, error) in
            if(error == nil)
            {
                self.showResponse(data!, response as! HTTPURLResponse, onFinish)
            }
            else
            {
                onFinish(nil)
            }
        }
        task.resume()
    }
    
    private func showResponse(_ data: Data,_ response: HTTPURLResponse, _ onFinish: ShowHandler)
    {
        if(response.statusCode == 200)
        {
            let enterprise = EnterpriseConverter.convert(json: data)
            onFinish(enterprise)
        }
        else
        {
            onFinish(nil)
        }
    }
    
}
