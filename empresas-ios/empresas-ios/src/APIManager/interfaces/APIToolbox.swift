//
//  AuthProtocol.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 25/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

protocol AuthProtocol
{
    func signIn(credentials: SignInCredentials, onFinish: @escaping SignInHandler)
}

protocol DataProtocol
{
    func enterpriseIndex(name: String, onFinish: @escaping EnterpriseIndexHandler)
    func enterpriseIndexWithFilter(params: EnterpriseParameters, onFinish: @escaping EnterpriseIndexHandler)
    func show(id: Int, onFinish: @escaping ShowHandler)
}

class APIToolbox
{
    static let shared = APIToolbox()
    
    public let auth: AuthProtocol
    public let data: DataProtocol
    
    init()
    {
        let manager = APIManager()
        auth = manager
        data = manager
    }
}
