//
//  EnterpriseData.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 25/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

struct Enterprise: Codable
{
    let enterprise: EnterpriseData
}

struct EnterpriseList: Codable
{
    let enterprises: [EnterpriseData]
}

struct EnterpriseData: Codable
{
    let id: Int
    let email_enterprise: String?
    let facebook: String?
    let twitter: String?
    let linkedin: String?
    let phone: String?
    let own_enterprise: Bool
    let enterprise_name: String
    let description: String
    let city: String
    let country: String
    let value: Int
    let share_price: Int
    let enterprise_type: EnterpriseType
}

struct EnterpriseType: Codable
{
    let id: Int
    let enterprise_type_name: String
}

class EnterpriseListConverter
{
    static func convert(json: Data) -> EnterpriseList
    {
        let decoder = JSONDecoder()
        do
        {
            let enterprises = try decoder.decode(EnterpriseList.self, from: json)
            return enterprises
        }
        catch
        {
            print(error.localizedDescription)
        }
        
        return EnterpriseList(enterprises: [])
    }
}

class EnterpriseConverter
{
    static func convert(json: Data) -> Enterprise?
    {
        let decoder = JSONDecoder()
        do
        {
            let enterprise = try decoder.decode(Enterprise.self, from: json)
            return enterprise
        }
        catch
        {
            print(error.localizedDescription)
        }
        
        return nil
    }
}
