//
//  EnterpriseParameters.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 24/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

struct EnterpriseParameters
{
    let enterpriseTypes: NSNumber
    let name: String
}
