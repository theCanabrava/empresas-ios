//
//  SearchViewController.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 25/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    let toolbox = APIToolbox.shared
    var enterpriseList: EnterpriseList?
    let cellReuseIdentifier = "cell"
    var selectedId: NSInteger?
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var resultCountLabel: UILabel!
    @IBOutlet weak var enterpriseTable: UITableView!
    
    @IBOutlet weak var backgroundTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var outterSpinner: UIImageView!
    @IBOutlet weak var innerSpinner: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        enterpriseTable.delegate = self
        enterpriseTable.dataSource = self
        loadSearchBar()
    }
    
    private func loadSearchBar()
    {
        searchField.attributedPlaceholder = NSAttributedString(string: "Empresa",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 0.4, green: 0.4, blue: 0.4, alpha: 0.7)])
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    @IBAction func startedEditingSearchField(_ sender: Any)
    {
        animateOnKeyboardShow()
    }
    
    private func animateOnKeyboardShow()
    {
        UIView.animate(withDuration: 0.4)
        {
            self.backgroundTopConstraint.constant = -127
            self.view.layoutIfNeeded()
        }
    }
    
    
    
    @IBAction func editedSearchView(_ sender: Any)
    {
        resignFirstResponder()
        guard let searchTerm = searchField.text else
        {
            return
        }
        
        if(searchTerm != "")
        {
            animateLoadingView()
            
            toolbox.data.enterpriseIndex(name: searchTerm)
            {
                (list) in
                DispatchQueue.main.async
                {
                    self.enterpriseList = list
                    self.enterpriseTable.reloadData()
                }
            }
        }
    }
    
    private func animateLoadingView()
    {
       loadingView.isHidden = false;
       self.rotateView(targetView: outterSpinner)
       self.rotateView(targetView: innerSpinner, duration: 0.5)
    }

    private func rotateView(targetView: UIView, duration: Double = 1.0, direction: Double = 1)
    {
       UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations:
       {
           targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
       })
       {
           finished in
           if(self.loadingView.isHidden == false)
           {
               self.rotateView(targetView: targetView, duration: duration)
           }
       }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 128
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        stopAnimatingLoadingView()
        if (enterpriseList != nil)
        {
            resultCountLabel.text = "\(enterpriseList!.enterprises.count) resultados encontrados"
            if(enterpriseList!.enterprises.count==0)
            {
                animaterOnNoResults()
            }
            return enterpriseList!.enterprises.count
        }
        
        resultCountLabel.text = ""
        return 0
    }
    
    private func stopAnimatingLoadingView()
    {
       DispatchQueue.main.async
       {
           self.loadingView.isHidden = true
           self.outterSpinner.layer.removeAllAnimations()
           self.innerSpinner.layer.removeAllAnimations()
       }
    }
    
    private func animaterOnNoResults()
    {
        UIView.animate(withDuration: 0.4)
        {
            self.backgroundTopConstraint.constant = 0
            self.resultCountLabel.text = "Nenhum resultado encontrado"
            self.view.layoutIfNeeded()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SearchTableViewCell = enterpriseTable.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! SearchTableViewCell
        cell.data = enterpriseList!.enterprises[indexPath.row]
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let searchCell = cell as! SearchTableViewCell
        searchCell.layoutTitle()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedId = enterpriseList!.enterprises[indexPath.row].id
        performSegue(withIdentifier: SegueConstants.enterpriseDataView, sender: self)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        guard let id = selectedId else
        {
            return
        }
        
        let enterpriseDataVC = segue.destination as! EnterpriseDataViewController
        enterpriseDataVC.id = id
        
    }
    
}
