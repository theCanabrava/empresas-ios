//
//  ViewController.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 24/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController
{
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var hidePasswordImage: UIImageView!
    
    @IBOutlet weak var gradientTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var welcomeLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var emailFieldImage: UIImageView!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var outterSpinner: UIImageView!
    @IBOutlet weak var innerSpinner: UIImageView!
    
    let toolbox = APIToolbox.shared
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        enterButton.layer.cornerRadius = 5
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedHideImage(tapGestureRecognizer:)))
        hidePasswordImage.isUserInteractionEnabled = true
        hidePasswordImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    

    @objc func tappedHideImage(tapGestureRecognizer: UITapGestureRecognizer)
    {
        passwordField.isSecureTextEntry = !passwordField.isSecureTextEntry
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    @IBAction func startEditingField(_ sender: Any)
    {
        if(isNotEditingFields())
        {
            animateOnKeyboardShow()
        }
    }
    
    private func isNotEditingFields() -> Bool
    {
        return welcomeLabel.isHidden == false
    }
    
    
    
    @IBAction func fillEmailField(_ sender: Any)
    {
        passwordField.becomeFirstResponder()
    }
    
    
    
    @IBAction func fillPasswordField(_ sender: Any)
    {
        resignFirstResponder()
        animaterOnKeyboardHide()
    }
    
    
    
    @IBAction func pressSignInButton(_ sender: Any)
    {
        guard let credentials = getCredentials() else
        {
            return
        }
        
        animateLoadingView()
        toolbox.auth.signIn(credentials: credentials)
        {
            (success) in
            self.stopAnimatingLoadingView()
            if(success)
            {
                self.handleSuccess()
            }
            else
            {
                self.handleFaillure()
            }
        }
    }
    
    private func getCredentials() -> SignInCredentials?
    {
        guard let email = emailField.text else
        {
            return nil
        }
        guard let password = passwordField.text else
        {
            return nil
        }
        
        return SignInCredentials(email: email, password: password)
    }
    
    private func animateLoadingView()
    {
        loadingView.isHidden = false;
        self.rotateView(targetView: outterSpinner)
        self.rotateView(targetView: innerSpinner, duration: 0.5)
    }
    
    private func rotateView(targetView: UIView, duration: Double = 1.0, direction: Double = 1)
    {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations:
        {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
        })
        {
            finished in
            if(self.loadingView.isHidden == false)
            {
                self.rotateView(targetView: targetView, duration: duration)
            }
        }
    }
    
    private func stopAnimatingLoadingView()
    {
        DispatchQueue.main.async
        {
            self.loadingView.isHidden = true
            self.outterSpinner.layer.removeAllAnimations()
            self.innerSpinner.layer.removeAllAnimations()
        }
    }
    
    private func handleSuccess()
    {
        DispatchQueue.main.async
        {
            self.performSegue(withIdentifier: SegueConstants.searchView, sender: self)
        }
    }
    
    private func handleFaillure()
    {
        DispatchQueue.main.async
        {
            UIView.animate(withDuration: 0.4)
            {
                self.colorBorder(self.emailField)
                self.colorBorder(self.passwordField)
                self.hidePasswordImage.image = UIImage.init(named: "error")
                self.emailFieldImage.image = UIImage.init(named: "error")
                self.errorLabel.text = "Credenciais incorretas"
                self.loadingView.isHidden = true
                self.view.layoutIfNeeded()
            }
        }
    }
    
    private func colorBorder(_ field: UITextField)
    {

        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor(red: 0.92, green: 0.34, blue: 0.34, alpha: 1).cgColor
        field.layer.cornerRadius = 5
    }
    
    
    
    private func animateOnKeyboardShow()
    {
        UIView.animate(withDuration: 0.4)
        {
            self.welcomeLabel.isHidden = true
            self.iconBottomConstraint.constant = 0
            self.iconTopConstraint.constant = 20
            self.gradientTopConstraint.constant = -100
            self.welcomeLabelBottomConstraint.constant = 40
            self.view.layoutIfNeeded()
        }
    }
    
    private func animaterOnKeyboardHide()
    {
        UIView.animate(withDuration: 0.4)
        {
            self.welcomeLabel.isHidden = false
            self.iconBottomConstraint.constant = 40
            self.iconTopConstraint.constant = 40
            self.gradientTopConstraint.constant = 0
            self.welcomeLabelBottomConstraint.constant = 80
            self.view.layoutIfNeeded()
        }
    }
}

