//
//  EnterpriseDataViewController.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 25/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import UIKit

class EnterpriseDataViewController: UIViewController
{
    var id: Int?
    let toolbox = APIToolbox.shared

    @IBOutlet weak var enterpriseTitle: UILabel!
    @IBOutlet weak var enterpriseDescription: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if(id != nil)
        {
            loadEnterprise()
            loadBackButton()
        }
    }
    
    func loadBackButton()
    {
        let yourBackImage = UIImage(named: "left_action")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)
    }
    
    private func loadEnterprise()
    {
        toolbox.data.show(id: id!)
        {
           (enterprise) in
            if (enterprise != nil)
            {
                DispatchQueue.main.async
                {
                    self.updateTexts(enterprise!)
                }
            }
        }
    }
    
    private func updateTexts(_ enterprise: Enterprise)
    {
        self.title = enterprise.enterprise.enterprise_name
        self.enterpriseTitle.text = enterprise.enterprise.enterprise_name
        pickBackgroundColor(enterprise)
        self.enterpriseDescription.text = enterprise.enterprise.description
    }
    
    private func pickBackgroundColor(_ enterprise: Enterprise)
    {
        let type = enterprise.enterprise.id
        if(type%3 == 0)
        {
            enterpriseTitle.backgroundColor = UIColor.init(red: 0.92, green: 0.59, blue: 0.59, alpha: 1)
        }
        else if(type%3 == 1)
        {
            enterpriseTitle.backgroundColor = UIColor.init(red: 0.56, green: 0.73, blue: 0.51, alpha: 1)
        }
        else
        {
            enterpriseTitle.backgroundColor = UIColor.init(red: 0.475, green: 0.733, blue: 0.792, alpha: 1)
        }
    }
    
}
