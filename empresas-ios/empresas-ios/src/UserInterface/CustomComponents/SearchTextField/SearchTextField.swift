//
//  SearchTextField.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 26/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import UIKit

class SearchTextField: UITextField
{
    let padding = UIEdgeInsets(top: 0, left: 42, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect
    {
        return bounds.inset(by: padding)
    }
}
