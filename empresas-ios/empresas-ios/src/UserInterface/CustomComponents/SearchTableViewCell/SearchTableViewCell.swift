//
//  SearchTableViewCell.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 26/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell
{
    var data: EnterpriseData?
    
    @IBOutlet weak var cellTitleLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    
    
    public func layoutTitle()
    {
        cellTitleLabel.text = data!.enterprise_name
        cellTitleLabel.layer.cornerRadius = 5
        cellTitleLabel.layer.masksToBounds = true
        pickBackgroundColor()
    }
    
    private func pickBackgroundColor()
    {
        let type = data!.id
        if(type%3 == 0)
        {
            cellTitleLabel.backgroundColor = UIColor.init(red: 0.92, green: 0.59, blue: 0.59, alpha: 1)
        }
        else if(type%3 == 1)
        {
            cellTitleLabel.backgroundColor = UIColor.init(red: 0.56, green: 0.73, blue: 0.51, alpha: 1)
        }
    }
}
