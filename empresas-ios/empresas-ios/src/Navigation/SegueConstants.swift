//
//  SegueConstants.swift
//  empresas-ios
//
//  Created by Victor Canabrava on 25/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import Foundation

struct SegueConstants
{
    static let searchView = "segueToSearchView"
    static let enterpriseDataView = "segueToEnterpriseDataView"
}
