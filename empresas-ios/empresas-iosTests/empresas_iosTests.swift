//
//  empresas_iosTests.swift
//  empresas-iosTests
//
//  Created by Victor Canabrava on 24/03/20.
//  Copyright © 2020 Victor Canabrava. All rights reserved.
//

import XCTest
@testable import empresas_ios

class empresas_iosTests: XCTestCase {

    override func setUp() {
        super.setUp()
        print("Set up")
    }

    override func tearDown() {
        super.tearDown()
        print("Tear down")
    }

    func testSignIn() {
        print("Started")
        let apiLib = APILibrary()
        let user = SignInCredentials(email: "testeapple@ioasys.com.br", password: "12341234")
        apiLib.requestSignIn(credentials: user) { (success) in
            XCTAssertEqual(success, true, "Should be signed in")
        }
    }

}
